# Stage_M2



## En construction 

        Pipeline d’Annotation Génomique 

    Stage de Master 2 BEE-GE 2024 

    Florian Blanchard - florian.bd.fb@gmail.com



Sommaire: 

1 - Utilisation du pipeline

2 - Contenue du pipeline 

3 - Ajout de nouvelles espèces

4 - Adapter le pipeline pour d’autres clades 

5 - Compléter le pipeline 



## 1- Utilisation du pipeline 

Commande pour lancer l’annotation 

/path/to/stage_m2/Script/snakemake --cores 16 abréviation_d_espèce

exemple : snakemake --cores 16 PKK  

## 2 - Contenue du pipeline 
## 3 - Ajout de nouvelles espèces
## 4 - Adapter le pipeline pour d’autres clades 
## 5 - Compléter le pipeline 
