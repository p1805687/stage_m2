#!/bin/bash

source /opt/src/miniconda3/etc/profile.d/conda.sh
conda activate braker3

species=${1}
threads=${2:-16}

Genome_Path=/home/fblanchard/stage_m2/Script/Genome_Path.txt
output=/home/fblanchard/stage_m2/Result/Trim/${species}/

lectureR1=$(grep ${species} ${Genome_Path} | cut -f 4).R1.fastq.gz
lectureR2=$(grep ${species} ${Genome_Path} | cut -f 4).R2.fastq.gz

#cree le repertoire sortie si besoin
if [ ! -d "${output}" ]; then
    mkdir -p "${output}"
fi

fastp --in1 ${lectureR1} --in2 ${lectureR2} \
        --out1 ${output}${species}.R1.fastq.gz \
        --out2 ${output}${species}.R2.fastq.gz \
        --unpaired1 ${output}.unpaired.clean.fastq.gz \
        --unpaired1 ${output}.unpaired.clean.fastq.gz \
        --detect_adapter_for_pe \
	    --thread ${threads} \
        --html /home/fblanchard/stage_m2/Result/Trim/${species}/${species}.htlm \
        --json /home/fblanchard/stage_m2/Result/Trim/${species}/${species}.json 
        