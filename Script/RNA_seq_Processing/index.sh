#!/bin/bash

source /opt/src/miniconda3/etc/profile.d/conda.sh
conda activate hisat2

species=${1}
threads=${2:-16} # Utilise 16 comme valeur par défaut

Genome_Path=/home/fblanchard/stage_m2/Script/Genome_Path.txt
path_index=/home/fblanchard/stage_m2/Result/Index/${species}
path_fasta=$(grep ${species} ${Genome_Path} | cut -f 3)

#cree le repertoire index si besoin
if [ ! -d "${path_index}" ]; then
    mkdir -p "${path_index}"
fi


#index
hisat2-build ${path_fasta} ${path_index}/genome -p ${threads}
