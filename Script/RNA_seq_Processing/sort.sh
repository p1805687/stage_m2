#!/bin/bash

species=${1}
threads=${2:-16} # Utilise 16 comme valeur par défaut

input=/home/fblanchard/stage_m2/Result/Mapping/${species}/${species}.sam
output=/home/fblanchard/stage_m2/Result/Mapping/${species}/${species}.sorted.bam
memory=$((256 / ${threads}))


#sort sam -> bam 
samtools sort ${input} -o ${output} -@ ${threads} -m ${memory}G
