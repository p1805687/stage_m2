#!/bin/bash

species=${1}
threads=${2:-16} # Utilise 16 comme valeur par défaut

input=/home/fblanchard/stage_m2/Result/Mapping/${species}/${species}.sorted.bam
output=/home/fblanchard/stage_m2/Result/Mapping/${species}/${species}.sorted.stat.txt


#stats
samtools stats ${input} -@ ${threads} > ${output}
