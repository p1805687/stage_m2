#!/bin/bash
source /opt/src/miniconda3/etc/profile.d/conda.sh
conda activate hisat2

species=${1}
threads=${2:-16} # Utilise 16 comme valeur par défaut

Genome_Path=/home/fblanchard/stage_m2/Script/Genome_Path.txt
index=/home/fblanchard/stage_m2/Result/Index/${species}/genome
output=/home/fblanchard/stage_m2/Result/Mapping/${species}
sortie=${output}/${species}.sam
sortie_stout=${output}/${species}_stdout.txt

lectureR1=/home/fblanchard/stage_m2/Result/Trim/${species}/${species}.R1.fastq.gz
lectureR2=/home/fblanchard/stage_m2/Result/Trim/${species}/${species}.R2.fastq.gz

#cree le repertoire sortie si besoin
if [ ! -d "${output}" ]; then
    mkdir -p "${output}"
fi

# Exécuter l'alignement HISAT2
hisat2 -x ${index} -1 ${lectureR1} -2 ${lectureR2} -S ${sortie} -p ${threads} >& ${sortie_stout}
