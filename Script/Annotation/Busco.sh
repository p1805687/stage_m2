#!/bin/bash
source /opt/src/miniconda3/etc/profile.d/conda.sh
conda activate braker3

species=${1}
threads=${2:-16} # Utilise 16 comme valeur par défaut

input=/home/fblanchard/stage_m2/Result/Annotation/${species}/braker.aa
output=/home/fblanchard/stage_m2/Result/Annotation/${species}/Busco
data=/home/fblanchard/stage_m2/Data
Genome_Path=/home/fblanchard/stage_m2/Script/Genome_Path.txt
path_fasta=$(grep ${species} ${Genome_Path} | cut -f 3)

#cree le repertoire output si besoin
if [ ! -d "${output}" ]; then
    mkdir -p "${output}"
fi


#Busco
busco -f -i ${input} -l arthropoda_odb10 --download_path ${data} --out_path ${output} -m prot -c ${threads} > ${output}/Busco_stout.txt

