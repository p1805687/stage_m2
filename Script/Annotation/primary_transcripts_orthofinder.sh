#!/bin/bash

species=${1}

python /home/fblanchard/stage_m2/Tools/OrthoFinder/tools/primary_transcript.py /home/fblanchard/stage_m2/Result/Annotation/OrthoFinder/OrthoFormat/${species}_braker_format.fa
