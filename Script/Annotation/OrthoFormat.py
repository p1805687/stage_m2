#!/usr/bin/env python3

#!/usr/bin/env python3

import os
import argparse

def update_fasta(species):
    input_file = f"/home/fblanchard/stage_m2/Result/Annotation/OrthoFinder/{species}_braker.fa"
    output_file = f"/home/fblanchard/stage_m2/Result/Annotation/OrthoFinder/OrthoFormat/{species}_braker_format.fa"
    
    with open(input_file, 'r') as f:
        with open(output_file, 'w') as out_f:
            for line in f:
                if line.startswith('>'):
                    gene_number = line.strip().split('.')[0][2:]
                    out_f.write(f"{line.strip()} gene:gene{gene_number}\n")
                else:
                    out_f.write(line)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Update FASTA file with species information')
    parser.add_argument('species', type=str, help='Name of the species')

    args = parser.parse_args()
    update_fasta(args.species)

