#!/bin/bash

#SBATCH --partition=normal
#SBATCH --time=10:00:00
#SBATCH --cpus-per-task=16
#SBATCH --mem=16G
#SBATCH --output=/beegfs/data/fblanchard/stage_m2/Result/OrthoFinder/std_output_orthofinder.txt
#SBATCH --error=/beegfs/data/fblanchard/stage_m2/Result/OrthoFinder/std_error_orthofinder.txt

orthofinder -f /beegfs/data/fblanchard/stage_m2/Result/primary_transcripts -I 2 -S diamond -A muscle -M msa -T iqtree -a 16 -t 16

