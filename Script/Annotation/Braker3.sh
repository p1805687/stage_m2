#!/bin/bash
source /opt/src/miniconda3/etc/profile.d/conda.sh
conda activate braker3

species=${1}
threads=${2:-16} # Utilise 16 comme valeur par défaut


Genome_Path=/home/fblanchard/stage_m2/Script/Genome_Path.txt
genome=$(grep ${species} ${Genome_Path} | cut -f 3)
bam=/home/fblanchard/stage_m2/Result/Mapping/${species}/${species}.sorted.bam
protDB=/home/fblanchard/stage_m2/Data/All_3base.fa
output=/home/fblanchard/stage_m2/Result/Annotation/${species}

#cree le repertoire output si besoin
if [ ! -d "${output}" ]; then
    mkdir -p "${output}"
fi

# Exporter PATH
export GENEMARK_PATH=/home/fblanchard/stage_m2/Tools/GeneMark-ETP/bin
export PROTHINT_PATH=/home/fblanchard/stage_m2/Tools/ProtHint-2.6.0/bin

braker.pl --genome=${genome} --prot_seq=${protDB} --bam=${bam} --threads ${threads} --gff3 --workingdir=${output} > ${output}/braker_stout.txt


cp ${output}/braker.aa /home/fblanchard/stage_m2/Result/Annotation/OrthoFinder/${species}_braker.fa
