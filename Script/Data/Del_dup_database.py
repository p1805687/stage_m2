#!/usr/bin/env python3

from collections import OrderedDict

def remove_duplicates(input_file, output_file):
    sequences = OrderedDict()

    with open(input_file, 'r') as infile:
        current_id = None
        current_sequence = ''
        for line in infile:
            if line.startswith('>'):
                if current_id is not None:
                    sequences[current_id] = current_sequence
                current_id = line.split()[0]
                current_sequence = line
            else:
                current_sequence += line

        if current_id is not None:
            sequences[current_id] = current_sequence

    with open(output_file, 'w') as outfile:
        for sequence in sequences.values():
            outfile.write(sequence)

if __name__ == "__main__":
    # Chemins des fichiers
    input_file = '/home/fblanchard/stage_m2/Data/All_EnsemblMetazoa.pep.all.fa'
    output_file = '/home/fblanchard/stage_m2/Data/All_EnsemblMetazoa.pep.all.clean.fa'

    # Appeler la fonction avec les chemins spécifiés
    remove_duplicates(input_file, output_file)

