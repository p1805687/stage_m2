#!/bin/bash

export targetsp=$1 ## what species we want to annotate
export assembly=$2 ## assembly 

export path=$3 ## root path of the directory in which we work
export pathTools=$4 ## path for Tools, including GeMoMa
export scheduler=$5 ## slurm or none

export version=... ## GeMoMa version

#########################################################################

### Modfy these paths as needed!!

export pathRNASeq=${path}/results/RNASeq_alignments/${targetsp}/${assembly}
export pathResults=${path}/results/GeMoMa_annotation/${targetsp}/${assembly}/GeMoMa
export pathScripts=${path}/Scripts/GeMoMa_annotation

#########################################################################

## create results directory

if [ -e ${pathResults} ]; then
    echo "results dir already there"
else
    mkdir -p ${pathResults}
fi

#########################################################################

echo "#!/bin/bash" > ${pathScripts}/sub_script_rnaseq_evidence

#############################################

if [ ${scheduler} = "slurm" ]; then
    echo "#SBATCH --job-name=gemoma_rna_seq_${targetsp}" >>  ${pathScripts}/sub_script_rnaseq_evidence
    echo "#SBATCH --output=${pathScripts}/std_output_GEMOMA_${targetsp}.txt" >>  ${pathScripts}/sub_script_rnaseq_evidence
    echo "#SBATCH --error=${pathScripts}/std_error_GEMOMA_${targetsp}.txt" >> ${pathScripts}/sub_script_rnaseq_evidence
    echo "#SBATCH --partition=normal" >> ${pathScripts}/sub_script_rnaseq_evidence
    echo "#SBATCH --mem=12G" >> ${pathScripts}/sub_script_rnaseq_evidence
    echo "#SBATCH --time=24:00:00" >> ${pathScripts}/sub_script_rnaseq_evidence
fi

#########################################################################

##### Modify BAM path!!!

echo "java  -Xms2G -Xmx64G -jar ${pathTools}/GeMoMa/GeMoMa-${version}.jar CLI ERE s=FR_UNSTRANDED m=${pathRNASeq}/accepted_hits_all_samples.bam outdir=${pathResults}" >> ${pathScripts}/sub_script_rnaseq_evidence

#########################################################################

if [ ${scheduler} = "slurm" ]; then
    sbatch ${pathScripts}/sub_script_rnaseq_evidence
else
    chmod a+x ${pathScripts}/sub_script_rnaseq_evidence
    ${pathScripts}/sub_script_rnaseq_evidence
fi

#########################################################################
