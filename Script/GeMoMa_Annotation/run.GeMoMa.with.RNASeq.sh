#!/bin/bash

export targetsp=$1
export targetassembly=$2
export refsp=$3
export refassembly=$4

export path=$5 ## root path of the directory in which we work
export pathTools=$6 ## path for Tools, including GeMoMa
export scheduler=$7 ## slurm or none

export version=... ## GeMoMa version

#########################################################################

export pathResults=${path}/results/genome_annotation/${target}/${assembly}/GeMoMa
export pathScripts=${path}/scripts/gene_annotation

## using mmseqs2, installed using apt on Ubuntu 20.04
## version 9-d36de+ds-4 amd64

#########################################################################

if [ -e ${pathResults}/${refsp} ]; then
    echo "results dir already there"
else
    mkdir -p ${pathResults}/${refsp}
fi

#########################################################################

### Cleanup fasta file if not already done
### Fasta headers cannot include spaces -> remove everything after the first space

export pathTargetGenome=.... ## include .clean.fa as a name

if [ -e ${pathTargetGenome} ]; then
    echo "cleaned-up genome file already there"
else
    perl ${pathScripts}/cleanup.fasta.names.pl --pathInput= ...  --pathOutput=${pathTargetGenome} 
fi

#########################################################################

### Define paths for reference genome abd annotation

export pathRefGenome=....
export pathRefAnnotation=...
 
#########################################################################
#########################################################################

if [ -e ${pathResults}/${refsp}/final_annotation.gff ]; then
    echo "already done"
else
    echo "#!/bin/bash" > ${pathScripts}/sub_script_gemoma_${refsp}

    #############################################

    ## prepare script
    
    if [ ${scheduler} = "slurm" ]; then
	echo "#SBATCH --job-name=gemoma_${refsp}" >>  ${pathScripts}/sub_script_gemoma_${refsp}
	echo "#SBATCH --output=${pathScripts}/std_output_GEMOMA_${refsp}.txt" >>  ${pathScripts}/sub_script_gemoma_${refsp}
	echo "#SBATCH --error=${pathScripts}/std_error_GEMOMA_${refsp}.txt" >> ${pathScripts}/sub_script_gemoma_${refsp}
	echo "#SBATCH --partition=normal" >> ${pathScripts}/sub_script_gemoma_${refsp}
	echo "#SBATCH --mem=12G" >> ${pathScripts}/sub_script_gemoma_${refsp}
	echo "#SBATCH --cpus-per-task=${threads}" >> ${pathScripts}/sub_script_gemoma_${refsp}
	echo "#SBATCH --time=24:00:00" >> ${pathScripts}/sub_script_gemoma_${refsp}
    fi

    ## actual command

    echo "java  -Xms2G -Xmx64G -jar ${pathTools}/GeMoMa/GeMoMa-${version}.jar CLI GeMoMaPipeline threads=${threads} outdir=${pathResults}/${refsp} GeMoMa.Score=ReAlign AnnotationFinalizer.r=NO o=true t=${pathTargetGenome} i=${refsp} a=${pathRefAnnotation} g=${pathRefGenome} GeMoMa.m=500000 Extractor.f=false GeMoMa.i=10  r=EXTRACTED introns=${pathResults}/introns.gff coverage=UNSTRANDED coverage_unstranded=${pathResults}/coverage_unstranded.bedgraph" >> ${pathScripts}/sub_script_gemoma_${refsp}

    ## now run script

    if [ ${scheduler} = "slurm" ]; then
	sbatch ${pathScripts}/sub_script_gemoma_${refsp}
    else
	chmod a+x ${pathScripts}/sub_script_gemoma_${refsp}
	${pathScripts}/sub_script_gemoma_${refsp}
    fi
fi

#########################################################################
