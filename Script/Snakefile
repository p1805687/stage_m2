# Snakefile

# Define the input and output files
species = "BCoP"  

# Define the number of threads 
threads_default = 16

# Rule all 
rule all:
    input:
	    "../Result/Annotation/BCoP/Busco/Busco_stout.txt"

# Rule to run index.sh
rule run_index:
    input:
        "Genome_Path.txt"
    output:
        "../Result/Index/{species}/genome.1.ht2"
    params:
        threads=threads_default
    shell:
        "bash RNA_seq_Processing/index.sh {wildcards.species} {params.threads}"

# Rule to run fastp.sh
rule run_fastp:
    input:
        "Genome_Path.txt"
    output:
        "../Result/Trim/{species}/{species}.R1.fastq.gz"
    params:
        threads=threads_default
    shell:
        "bash RNA_seq_Processing/fastp.sh {wildcards.species} {params.threads}"

# Rule to run mapping.sh
rule run_mapping:
    input:
        "Genome_Path.txt",
        "../Result/Trim/{species}/{species}.R1.fastq.gz",
        "../Result/Index/{species}/genome.1.ht2"
    output:
        "../Result/Mapping/{species}/{species}.sam"
    params:
        threads=threads_default
    shell:
        "bash RNA_seq_Processing/mapping.sh {wildcards.species} {params.threads}"

# Rule to run sort.sh
rule run_sort:
    input:
        "../Result/Mapping/{species}/{species}.sam"
    output:
        "../Result/Mapping/{species}/{species}.sorted.bam"
    params:
        threads=threads_default
    shell:
        "bash RNA_seq_Processing/sort.sh {wildcards.species} {params.threads}"

# Rule to run stat.sh
rule run_stat:
    input:
        "../Result/Mapping/{species}/{species}.sorted.bam"
    output:
        "../Result/Mapping/{species}/{species}.sorted.stat.txt"
    params:
        threads=threads_default
    shell:
        "bash RNA_seq_Processing/stat.sh {wildcards.species} {params.threads}"

# Rule to run Braker3.sh
rule run_Braker3:
    input:
        "../Result/Mapping/{species}/{species}.sorted.bam"
    output:
        "../Result/Annotation/{species}/braker.aa"
    params:
        threads=threads_default
    shell:
        "bash Annotation/Braker3.sh {wildcards.species} {params.threads}"

# Rule to run Busco.sh
rule run_Busco:
    input:
        "../Result/Annotation/{species}/braker.aa"
    output:
        "../Result/Annotation/{species}/Busco/Busco_stout.txt"
    params:
        threads=threads_default
    shell:
        "bash Annotation/Busco.sh {wildcards.species} {params.threads}"
